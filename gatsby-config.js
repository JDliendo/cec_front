/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

const path = require("path")

console.log(__dirname)
module.exports = {
  /* Your site config here */
  // in gatsby-config.js
  plugins: [
    {
      resolve: "gatsby-plugin-scss-typescript",
      options: {
        cssLoaderOptions: {
          importLoaders: 1,
          localIdentName: "[name]_[local]___[hash:base64:5]_[emoji:1]",
        },
        sassLoaderOptions: {
          test: /\.s[ac]ss$/i,
          includePaths: [path.resolve(__dirname, "./src/")],
        },
        cssMinifyOptions: {
          assetNameRegExp: /\.optimize\.css$/g,
          canPrint: true,
        },
        cssExtractOptions: {
          filename: "[name].css",
          chunkFilename: "[id].css",
        },
      },
    },
    {
      resolve: "gatsby-plugin-root-import",
      options: {
        src: path.join(__dirname, "src/"),
      },
    },
  ],
}
