import React, { FC } from "react"
import MainContainer from "../components/MainContainer"

const Home: FC = () => {
  return <MainContainer>
      <h1>Hello World</h1>
  </MainContainer>
}

export default Home
