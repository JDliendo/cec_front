import React, { FC } from "react"
import MainContainer from "../../components/MainContainer"
import Field from "../../components/Field"
import Button from "../../components/Button"
import { Formik, Form, FormikHelpers } from "formik"
import * as Yup from "yup"

import { passwordValidationYup } from "../../util/validations"
import Box from "@material-ui/core/Box"

const formSchema = Yup.object().shape({
  nombre: Yup.string().required("Campo Requerido"),
  apellido: Yup.string().required("Campo Requerido"),
  email: Yup.string()
    .required("Campo Requerido")
    .email("Correo Electronico Invalido"),
  contraseña: passwordValidationYup,
  confirmarContraseña: passwordValidationYup,
})

interface Values {
  nombre: string
  apellido: string
  email: string
  contraseña: string
  confirmarContraseña: string
}

const SignUp: FC = () => {
  return (
    <MainContainer>
      <h1>Registrarse</h1>
      <Formik
        initialValues={{
          nombre: "",
          apellido: "",
          email: "",
          contraseña: "",
          confirmarContraseña: "",
        }}
        validationSchema={formSchema}
        onSubmit={(
          values: Values,
          { setSubmitting }: FormikHelpers<Values>
        ) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2))
            setSubmitting(false)
          }, 500)
        }}
      >
        <Form>
          <Box
            flexDirection="column"
            display="flex"
            justifyContent="center"
            alignContent="center"
          >
            <Field label="Nombre" name="name" />

            <Field label="Apellido" name="lastName" />

            <Field label="Email" name="email" type="email" />

            <Field label="Contraseña" name="password" type="password" />

            <Field
              label="Confirme Contraseña"
              name="password"
              type="password"
            />

            <Button type="submit">Registrarse</Button>
          </Box>
        </Form>
      </Formik>
    </MainContainer>
  )
}

export default SignUp
