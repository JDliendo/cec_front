import React, { FC } from "react"
import MainContainer from "../../components/MainContainer"
import Button from "../../components/Button"
import Field from "../../components/Field"
import { Form, FormikHelpers, Formik } from "formik"
import * as Yup from "yup"
import { passwordValidationYup } from "../../util/validations"
import Box from "@material-ui/core/Box"

const formSchema = Yup.object().shape({
  email: Yup.string()
    .required("Campo Requerido")
    .email("Correo Electronico Invalido"),
  contraseña: passwordValidationYup,
})

interface Values {
  email: string
  contraseña: string
}

const SignIn: FC = () => {
  return (
    <MainContainer>
      <h1>Iniciar sesión</h1>
      <Formik
        initialValues={{
          email: "",
          contraseña: "",
        }}
        validationSchema={formSchema}
        onSubmit={(
          values: Values,
          { setSubmitting }: FormikHelpers<Values>
        ) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2))
            setSubmitting(false)
          }, 500)
        }}
      >
        <Form>
          <Box
            flexDirection="column"
            display="flex"
            justifyContent="center"
            alignContent="center"
          >
            <Field id="email" name="email" type="email" label="Email" />
            <Field
              label="Contraseña"
              id="password"
              name="password"
              type="password"
            />
            <Button type="submit">Iniciar sesión</Button>
          </Box>
        </Form>
      </Formik>
    </MainContainer>
  )
}

export default SignIn
