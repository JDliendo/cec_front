import React, { FC} from 'react';
import MUButton, {ButtonProps as MUButtonProps} from '@material-ui/core/Button';

interface ButtonProps extends MUButtonProps
{
    
}

const Button: FC<ButtonProps> = (props) => {
    return(<MUButton 
    variant="contained"
    {...props}>
        {props.children}
    </MUButton>)
};


export default Button;