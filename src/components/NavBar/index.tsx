import React, { FC } from "react"
import BottomNavigation, {
  BottomNavigationProps,
} from "@material-ui/core/BottomNavigation"
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction"
import RestoreIcon from "@material-ui/icons/Restore"
import FavoriteIcon from "@material-ui/icons/Favorite"
import PermIdentityIcon from "@material-ui/icons/PermIdentity"
import { navigate } from 'gatsby'
export type NavBarProps = BottomNavigationProps & { mode: "admin" }

const NavBar: FC<NavBarProps> = ({ value,  }) => {
  return (
    <BottomNavigation
      style={{
        position: "fixed",
        width: "100%",
        bottom: 0,
      }}
      value={value}
      onChange={(event, newValue) => {
        navigate(newValue);
      }}
    >
      <BottomNavigationAction value="/recientes" icon={<RestoreIcon />} />
      <BottomNavigationAction value="/materias" icon={<FavoriteIcon />} />
      <BottomNavigationAction value="/perfil" icon={<PermIdentityIcon />} />
    </BottomNavigation>
  )
}

export default NavBar
