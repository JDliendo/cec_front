import React, { FC } from "react"
import MUTextField, {
  TextFieldProps as MUTextFieldProps,
} from "@material-ui/core/TextField"
import { Field as FField } from "formik"
export type FieldProps = MUTextFieldProps

const Field: FC<FieldProps> = props => {
  return <FField {...props} component={MUTextField} />
}

export default Field
