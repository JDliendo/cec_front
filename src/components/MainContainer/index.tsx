import React, { FC } from "react"
import * as classes from "./MainContainer.module.scss"
import Box from "@material-ui/core/Box"
import NavBar from "../NavBar/index"

const MainContainer: FC = ({ children }) => {
  return (
    <Box
      style={{ padding: 0, margin: 0, minHeight: "100vh" }}
      className={classes.red}
    >
      {children}
      <NavBar mode="admin" />
    </Box>
  )
}
export default MainContainer
