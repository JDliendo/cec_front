import capi from "./CECAPI"

export const isBrowser = () => typeof window !== "undefined"

export const getUser = () =>
  isBrowser() && window.localStorage.getItem("gatsbyUser")
    ? JSON.parse(window.localStorage.getItem("gatsbyUser") || "")
    : {}

const setUser = (user : any) =>
  window.localStorage.setItem("gatsbyUser", JSON.stringify(user))

export const handleLogin = ({ email, password }:{email:string, password:string}) => {
 
  const req = capi.post('/sign-in',{
    email,
    password
  })

  return req;
}

export const handleSignUp = ({ email, password, name}:{name:string, email:string, password:string}) => {
 
  const req = capi.post('/sign-up',{
    email,
    password,
    name
  })

  return req;
}

export const isLoggedIn = () => {
  const user = getUser()

  return !!user.username
}

export const logout = (callback: any) => {
  setUser({})
  callback()
}