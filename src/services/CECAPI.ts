import {create} from 'apisauce'

const capi = create({
    baseURL: 'https://aqueous-fortress-19492.herokuapp.com',
})

export default capi;