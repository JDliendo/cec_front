import * as Yup from "yup"

export const passwordValidationYup = Yup.string()
  .required("Campo Requerido")
  .min(6, "Mínimo 6 caracteres")
  .max(15, "Máximo 15 caracteres")
  .matches(
    /^(?=.*[A-Za-z])(?=.*d)(?=.*[@$!%*#?&])[A-Za-zd@$!%*#?&]{6,}$/,
    "Debe contener al menos 6 caracteres, una mayúscula, un número y un símbolo"
  )
